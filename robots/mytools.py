from IPython.core.magic import register_cell_magic
import xml.etree.ElementTree as ET
import os
import xmlrep

# robot cell magic
@register_cell_magic
def robot(line, cell):
    fout = open('tmp.rob', 'w')
    fout.write(cell)
    fout.close()
    os.system('robot --quiet -o output.xml tmp.rob')
    xmlrep.report('output.xml')