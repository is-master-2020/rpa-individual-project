import os
import re
import csv
import urllib.parse
from typing import List
from robot.api.logger import info, debug, trace, console

replaceVideoControls = re.compile("Play Video", re.IGNORECASE)
outputfile = './output/amazon_reviews.csv'

def get_first_child_by_tag(element, tag):
    actualLink = element.find_element_by_tag_name(tag)
    return actualLink

def get_children_by_class(element, classname):
    children = element.find_elements_by_class_name(classname) # element.find_elements_by_xpath('./div[contains(@class, "' + classname + '")]')
    return children

def get_children_by_tag(element, tag):
    children = element.find_elements_by_tag_name(tag)
    return children

def get_xpath_children_by_tag(element, tag):
    children = element.find_elements_by_xpath('./' + tag + '')
    return children




def get_rating_element_from_raw(element):
    return element.find_element_by_xpath('./div/div/div/a/i[@data-hook="review-star-rating"]/parent::*')

def get_old_rating_element_from_raw(element):
    return element.find_element_by_xpath('./div/div/div/i[@data-hook="cmps-review-star-rating"]')

def get_title_element_from_raw(element):
    return element.find_element_by_xpath('./div/div/div/*[@data-hook="review-title"]')

def get_content_element_from_raw(element):
    return element.find_element_by_xpath('./div/div/div/span[@data-hook="review-body"]')



# next come the python helper functions that are so primitive, as to effectively demonstrate how horrible the robotframework is to use:

def create_hashset():
    return set()

def add_to_set(sset, element):
    sset.add(element)
    return sset

def get_true():
    return True

def get_false():
    return False

def get_len(listlike):
    return len(listlike)

def get_number(number):
    return number

def create_list2d():
    mylist = [['rating', 'title', 'content', 'productName', 'productUrl']]
    return mylist

def get_a_counter():
    return 0

def increment_counter(counter):
    return counter + 1

def add_empty_to_list2d(mylist):
    mylist.append(['', '', '', '', ''])
    return

def add_rating_to_list2d(mylist, rating, counter):
    mylist[counter][0] = rating.strip()
    return

def add_title_to_list2d(mylist, title, counter):
    mylist[counter][1] = title.strip()
    return

def add_content_to_list2d(mylist, content, counter):
    mylist[counter][2] = replaceVideoControls.sub("", content.strip())
    return

def add_title_and_more_to_list2d(mylist, title, productTitle, href):
    mylist.append(['', ''+title.strip(), '', ''+productTitle.strip(), ''+href.strip()])
    return




# initially, clear the file and write the header row
def write_initial_row_to_csv():
   
    with open(outputfile, 'w', encoding='UTF8') as f:
        f.truncate()

        writer = csv.writer(f)  #, delimiter=',', lineterminator='\n'
        writer.writerow(['rating', 'title', 'content', 'productTitle', 'reviewHref'])
        return

def write_row_to_csv(rating, title, content, productTitle, reviewHref):
   
    with open(outputfile, 'a+', encoding='UTF8') as f:
        writer = csv.writer(f)
        writer.writerow([
            '' + rating.strip()[0],
            '' + title.strip(),
            replaceVideoControls.sub("", content.strip()),
            '' + productTitle.strip(),
            '' + reviewHref.strip()
        ])
        return




def string_contains(stri, substrToSearchFor):
    return substrToSearchFor in stri

def extract_old_star_rating_from_class_name(classname):
    return str(re.findall("a\-star\-([0-5])", classname)[0])




def get_search_url_on_amazon_for(searchterm):
    return 'https://www.amazon.com/s?k=' + urllib.parse.quote_plus(searchterm) + '&language=en_US'
