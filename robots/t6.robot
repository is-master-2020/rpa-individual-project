*** Settings ***
Library             SeleniumLibrary
Library             Collections
Library             MyLib.py
Resource            config.resource
Resource            keywords.resource



*** Test Cases ***

# To have english reviews we search over  amazon.com not amazon.de
Fetch All Reviews

    ${searchResultsPageUrl}=        Get Search URL On Amazon For        ${AMAZON_PRODUCT_SEARCH_TERM}
    Log To Console                  Search results page is ${searchResultsPageUrl}
    Open Browser                    ${searchResultsPageUrl}     Firefox


    # initialize csv file with headers
    Write Initial Row to Csv

    Log To Console                  Now Sleeping
    Sleep                           ${WAITING_TIME_ON_EACH_PAGE}
    Wait Until Element Is Visible   //div[contains(@class, "s-title-instructions-style")]//h2//a
    ${resultsRaw}=                  Get WebElements         //div[contains(@class, "s-title-instructions-style")]//h2//a


    # loop through product results on first page,
    # call subfunction on each link to extract its reviews.
    ${productCounter}=              Get A Counter
    Log To Console                  Loop Through products
    FOR   ${el}    IN    @{resultsRaw}

        IF      ${productCounter} < ${MAX_PRODUCT_COUNT_TO_FETCH}
            ${txt}=             Get Text                ${el}
            ${href}=            Get Element Attribute   ${el}     href
            Log To Console      Product: ${txt}
            Log To Console      Product link: ${href}

            TRY
                Sleep   ${WAITING_TIME_ON_EACH_PAGE}
                Collect Amazon Reviews From Product Page And Write To Csv    ${href}        ${txt}
            EXCEPT
                Log To Console      Could not fetch reviews from this one :(
            END

            ${productCounter}=         Increment Counter       ${productCounter}
        END

    END


    [Teardown]      Close All Browsers