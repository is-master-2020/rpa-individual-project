from flask import Flask, render_template, request, redirect
from flask_bootstrap import Bootstrap
from waitress import serve

# more specific libraries for our functions
import sqlite3
import pickle
import math


# bootstrap the frontend app
app = Flask(__name__)
Bootstrap(app)


#define standard routes for our three pages.
@app.route("/")
def credit():
	return redirect("/home", code=302)


@app.route("/home")
def home():
	return render_template("index.html")


@app.route("/reviews")
def reviews():
	return render_template("reviews.html")


@app.route("/prediction")
def prediction():
	return render_template("prediction.html")






############# functions for interacting with the database ###################################



# loads the prediction for a star rating, based on the pickled machine learned model
@app.route('/load_prediction_rating/<title>', methods=["GET"])
def load_prediction_rating(title):

	# load the pickled model.
	fullpackage = pickle.load(open( "robots/review_classifier_package.ml", "rb" ) )
	vctr = fullpackage['vectorizer']
	classifier = fullpackage['classifier']
	upperbound = fullpackage['text-length-upperbound']
	title_capped = title[0:upperbound]


	# make a sophisticated prediction.
	# by vectorizing, TODO PUNCTUATION, prediction
	encoding = vctr.transform([title_capped])
	prediction = classifier.predict(encoding[0])

	return str(prediction[0]) + ' out of 5 stars!'


# loads the count of how many reviews are even there in the database.
@app.route('/load_reviews_count/', methods=["GET"])
def load_reviews_count():

	# connect to local embedded database
	database = "./robots/db/amazon_reviews.sqlite"
	conn = sqlite3.connect(database)

	# make a query to retrieve the count of reviews in the database
	cur = conn.cursor()
	count = cur.execute("select count(*) from Reviews").fetchone()
	conn.close()
	return str(count[0])


# loads all the reviews into an html tbody
@app.route('/load_review_data/<limit>/<offset>', methods=["GET"])
def load_review_data(limit, offset):

	# connect to local embedded database
	database = "./robots/db/amazon_reviews.sqlite"
	conn = sqlite3.connect(database)
	html = ""


	# make a query to retrieve all reviews at once.
	# and append that data to the html table string.
	cur = conn.cursor()
	data = cur.execute("select rating, title, content from Reviews limit " + limit + " offset " + offset).fetchall()
	for row in data:
		html += """
				<tr>
					<td>%s</td>
					<td>%s</td>
					<td>%s</td>
				</tr>
		""" % row

	conn.close()
	return html


# loads html pagination. Based on a total number of items and number of items per page.
@app.route('/load_pagination/<totalnumber>/<countperpage>', methods=["GET"])
def load_pagination(totalnumber, countperpage):

	numberOfPages = math.ceil(float(totalnumber) / float(countperpage))

	# make an html pagination, based on number of pages
	html = ""
	for i in range(numberOfPages):
		html += """
				<li class="page-item" style="cursor: pointer">
					<a class="page-link">%s</a>
				</li>
		""" % (i+1)

	return html


# in the lecture we got taught 
serve(app, host='127.0.0.1', port=8080)
